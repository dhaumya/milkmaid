<!DOCTYPE html>
<html>
<head>
  <title>Customers - MilkMaid</title>
  <link rel="icon" type="image/x-icon" href="../milkmaid-logo.png">
  <link rel="stylesheet" href="../CSS/common.css">
</head>
<body>
<button class="backred" onclick="location.href = '../';">←</button>
<div class="main">
<h2>My First JavaScript</h2>
<button type="button" onclick="location.href = 'NewCustomer/';">New Customers</button>

<p id="demo"></p>
<?php
include '../decode.php';
// $jsonobj = file_get_contents('../info.json');
// $arr = json_decode($jsonobj, true);
//
// $servername = $arr["servername"];
// $username = $arr["username"];
// $password = $arr["password"];
// echo "<br>";

// $servername = "localhost";
// $username = "milkmaid";
// $password = "passmaid";
// $dbname = "dbtestmaid";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
  die("Connection failed: " . $conn->connect_error);
}

$sql = "SELECT CUS_ID, name, address, rateformula, email, phonenumber FROM customers";
$result = $conn->query($sql);

echo "<table><tr><th>ID</th><th>Name</th><th>Address</th><th>RateFormula</th><th>Email</th><th>Phone</th></tr>";

if ($result->num_rows > 0) {
  // output data of each row
  while($row = $result->fetch_assoc()) {
    echo "<tr><td>" . $row["CUS_ID"]. "</td><td>" . $row["name"]. "</td><td>" . $row["address"]. "</td><td>" . $row["rateformula"]. "</td><td>" . $row["email"]. "</td><td>" . $row["phonenumber"]. "</td><tr>";
  }
} else {
  echo "0 results";
}

echo "</table>";

$conn->close();
?>
</div>
</body>
</html>
