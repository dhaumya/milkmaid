<!DOCTYPE html>
<html>
<head>
  <title>Rate Lists - MilkMaid</title>
  <link rel="icon" type="image/x-icon" href="../milkmaid-logo.png">
  <link rel="stylesheet" href="../CSS/common.css">
</head>
<body>
<button class="backred" onclick="location.href = '../';">←</button>
<div class="main">
  <math>
    Rate = 7*(fat) - 0.1(26-(clr))
  </math><br>
  <form>
    CLR: <input type="number" onkeyup="showHint(this.value)">
  </form><br>
  <p>Rate: <span id="output"></span>Rs/lit</p>
  <script>
    function showHint(str) {
      if (str.length == 0) {
        document.getElementById("output").innerHTML = "";
        return;
      } else {
        const xmlhttp = new XMLHttpRequest();
        xmlhttp.onload = function() {
          document.getElementById("output").innerHTML = this.responseText;
        }
        xmlhttp.open("GET", "output.php?q=" + str);
        xmlhttp.send();
      }
  }
  </script><br>
  <button type="button" onclick="location.href = 'NewRateList/';">New RateList</button>
</div>
</body>
</html>
