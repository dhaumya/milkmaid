<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
  <meta charset="utf-8">
  <title>Setup - MilkMaid</title>
  <link rel="icon" href="../milkmaid-logo.png">
  <link rel="stylesheet" href="../CSS/common.css">
  <link rel="stylesheet" href="../CSS/form.css">
</head>
<body>
<div class="flexBox">
  <form id="setupForm" class="inputform" action="Working/" method="post">
    <div class="baseFlex">
      <button type="button" id="prevBtn" onclick="next(-1)">Previous</button>
      <button type="button" id="nextBtn" style="margin-left: auto;" onclick="next(1)">Next</button>
    </div>

    <div class="tab">
      <h1 class="heading">Setup:</h1>
      <p>Be sure to follow along the wiki.</p>
    </div>

    <div class="tab">
      <h1 class="heading">Database:</h1>
      <p><input type="text" class="required" name="dbServerName" placeholder="Servername..." oninput="this.classList.remove('invalid');"></p>
      <p><input type="text" class="required" name="dbUserName" placeholder="Username..." oninput="this.classList.remove('invalid');"></p>
      <p><input type="password" class="required" id="dbPassword" name="dbPassword" placeholder="Password..." oninput="this.classList.remove('invalid');"><input type="checkbox" style="margin: 0 0 0 -15px;" onclick="togglePassword('dbPassword');"></p>
      <p><input type="text" class="required" name="dbName" placeholder="Database Name..." oninput="this.classList.remove('invalid');"></p>
    </div>

    <div class="tab">
      <h1 class="heading">Admin Account Details:</h1>
      <p><input type="text" class="required" name="firstName" placeholder="First Name..." oninput="this.classList.remove('invalid');"><input type="text" class="required" name="lastName" placeholder="Last Name..." oninput="this.classList.remove('invalid');"></p>
      <p><input type="text" class="required" name="userName" placeholder="Username..." oninput="this.classList.remove('invalid');"></p>
      <p><input type="password" class="required" id="password" name="password" placeholder="Password..." oninput="this.classList.remove('invalid');"><input type="checkbox" style="margin: 0 0 0 -15px;" onclick="togglePassword('password');"></p>
      <p><input type="password" class="required" id="cfPassword" name="cfPassword" placeholder=" Confirm Password..." oninput="this.classList.remove('invalid');confPass();"><input type="checkbox" style="margin: 0 0 0 -15px;" onclick="togglePassword('cfPassword');"></p>
    </div>

    <div class="tab">
      <h1 class="heading">Company Details:</h1>
      <p><input type="text" class="required" name="compName" placeholder="Company Name..." oninput="this.classList.remove('invalid');"></p>
      <p><input type="text" class="required" name="compCity" placeholder="Company City..." oninput="this.classList.remove('invalid');"></p>
      <p><input type="file" name="compIcon" oninput="this.className = ''"></p>
    </div>

    <div class="tab">
      <h1 class="heading">Presets:</h1>
      <select class="required" name="preset">
        <option value="arch">Arch Linux</option>
        <option value="debian" disabled>Debian Linux</option>
        <option value="custom" disabled>Custom...</option>
      </select>
    </div>
  </form>
  <script>
    function confPass() {
      let valid1 = true;
      const p = document.getElementById('password');
      const c = document.getElementById('cfPassword');
      if (p.value != c.value) {
        c.classList.add("invalid");
        valid1 = false;
      } else {
        c.classList.remove("invalid");
        valid1 = true;
      }
      return valid1;
    }

    function togglePassword(id) {
      const p = document.getElementById(id);
      if (p.type === "text") {
        p.type = "password";
      } else {
        p.type = "text";
      }
    }

    let currentTab = 0;
    showTab(currentTab);

    function showTab(n) {
      const cT = document.getElementsByClassName("tab");
      cT[n].style.display = "flex";
      if (n == 0) {
        document.getElementById("nextBtn").innerHTML = "Start";
        document.getElementById("prevBtn").style.display = "none";
      } else if (n == (cT.length - 1)) {
        document.getElementById("nextBtn").innerHTML = "Submit";
      } else {
        document.getElementById("prevBtn").style.display = "block";
        document.getElementById("nextBtn").innerHTML = "Next";
      }
    }

    function next(n) {
      const cT = document.getElementsByClassName("tab");
      if (!validateForm()) {return false;}
      cT[currentTab].style.display = "none";
      currentTab = currentTab + n;
      if (currentTab == cT.length) {
        document.getElementById("setupForm").submit();
        return false;
      }
      showTab(currentTab);
    }

    function validateForm() {
      let cT, rCT, i, valid = true;
      cT = document.getElementsByClassName("tab");
      rCT = cT[currentTab].getElementsByClassName("required");

      for (let i = 0; i < rCT.length; i++) {
        if (rCT[i].value == "") {
          rCT[i].classList.add("invalid");
          valid = false;
        }
      }

      if (!confPass()) {
        valid = false;
      }
      return valid;
    }
  </script>
</div>
</body>
</html>
