<?php

if ($_SERVER["REQUEST_METHOD"] == "POST") {
  $dbservername = test_input($_POST["dbservername"]);
  $dbusername = test_input($_POST["dbusername"]);
  $dbpassword = test_input($_POST["dbpassword"]);
  $dbname = test_input($_POST["dbname"]);
  $firstName = test_input($_POST["firstName"]);
  $lastName = test_input($_POST["lastName"]);
  $userName = test_input($_POST["userName"]);
  $password = test_input($_POST["password"]);
  $compName = test_input($_POST["compName"]);
  $compAddr = test_input($_POST["compAddr"]);
  $preset = test_input($_POST["preset"]);
}

$compIcon = $_POST["compIcon"];

function test_input($data) {
  $data = trim($data);
  $data = stripslashes($data);
  $data = htmlspecialchars($data);
  return $data;
}

// TEST DISABLE THIS
$dbservername = "localhost";
$dbusername = "usertest";
$dbpassword = "passtest";
$dbname = "test1";

// MySQL
$conn = new mysqli($dbservername, $dbusername, $dbpassword, $dbname);
if ($conn->connect_error) {
  die("MySQL Connection failed: ".$conn->connect_error);
} else {
  echo "MySQL Connected Successfully.<br />";
}

$acc = "CREATE TABLE accounts (acc_id int NOT NULL AUTO_INCREMENT, priority int NOT NULL DEFAULT '100', first_name varchar(25) NOT NULL, last_name varchar(25) NOT NULL, username varchar(255) NOT NULL, password varchar(255) NOT NULL, PRIMARY KEY (acc_id), UNIQUE (username));";
if ($conn->query($acc) === TRUE) {
  echo "MySQL: Created Table <b>Accounts</b> Successfully.<br />";
} else {
  echo "Error: ".$acc."<br>".$conn->error;
}

$pdt = "CREATE TABLE products (pdt_id int NOT NULL AUTO_INCREMENT, name varchar(225) NOT NULL, price varchar(255) NOT NULL, PRIMARY KEY (pdt_id), UNIQUE (name));";
if ($conn->query($pdt) === TRUE) {
  echo "MySQL: Created Table <b>Products</b> Successfully.<br />";
} else {
  echo "Error: ".$pdt."<br>".$conn->error;
}

$sup = "CREATE TABLE supplies (sup_id int NOT NULL AUTO_INCREMENT, name varchar(225) NOT NULL, price varchar(255) NOT NULL, PRIMARY KEY (sup_id), UNIQUE (name));";
if ($conn->query($sup) === TRUE) {
  echo "MySQL: Created Table <b>Supplies</b> Successfully.<br />";
} else {
  echo "Error: ".$sup."<br>".$conn->error;
}

$rate = "CREATE TABLE ratelists (rl_id int NOT NULL AUTO_INCREMENT, df_clr int NOT NULL, fat_rate int NOT NULL, PRIMARY KEY (rl_id));";
if ($conn->query($rate) === TRUE) {
  echo "MySQL: Created Table <b>Rates</b> Successfully.<br />";
} else {
  echo "Error: ".$rate."<br>".$conn->error;
}

$cus = "CREATE TABLE customers (cus_id int NOT NULL AUTO_INCREMENT, first_name varchar(255) NOT NULL, last_name varchar(255) NOT NULL, city varchar(255) NOT NULL, rl_id int NOT NULL, email varchar(255), phone varchar(10), balance decimal(10,5) NOT NULL DEFAULT '0', PRIMARY KEY (cus_id), FOREIGN KEY (rl_id) REFERENCES ratelists(rl_id) ON DELETE RESTRICT ON UPDATE RESTRICT, CONSTRAINT person UNIQUE (first_name,last_name));";
if ($conn->query($cus) === TRUE) {
  echo "MySQL: Created Table <b>Customers</b> Successfully.<br />";
} else {
  echo "Error: ".$cus."<br>".$conn->error;
}

$ven = "CREATE TABLE vendors (ven_id int NOT NULL AUTO_INCREMENT, first_name varchar(255) NOT NULL, last_name varchar(255) NOT NULL, city varchar(255) NOT NULL, email varchar(255), phone varchar(10), balance decimal(10,5) NOT NULL DEFAULT '0', PRIMARY KEY (ven_id), CONSTRAINT person UNIQUE (first_name,last_name));";
if ($conn->query($ven) === TRUE) {
  echo "MySQL: Created Table <b>Vendors</b> Successfully.<br />";
} else {
  echo "Error: ".$ven."<br>".$conn->error;
}

$sal = "CREATE TABLE sales (sal_id int NOT NULL AUTO_INCREMENT, cus_id int NOT NULL, pdt_id int NOT NULL, ern_id int NOT NULL, quantity int NOT NULL, money decimal(10,5) NOT NULL, PRIMARY KEY (sal_id), FOREIGN KEY (cus_id) REFERENCES customers(cus_id) ON DELETE RESTRICT ON UPDATE RESTRICT, FOREIGN KEY (pdt_id) REFERENCES products(pdt_id) ON DELETE RESTRICT ON UPDATE RESTRICT);";
if ($conn->query($sal) === TRUE) {
  echo "MySQL: Created Table <b>Sales</b> Successfully.<br />";
} else {
  echo "Error: ".$sal."<br>".$conn->error;
}

$pur = "CREATE TABLE purchases (pur_id int NOT NULL AUTO_INCREMENT, ven_id int NOT NULL, sup_id int NOT NULL, spn_id int NOT NULL, quantity int NOT NULL, money decimal(10,5) NOT NULL, PRIMARY KEY (pur_id), FOREIGN KEY (ven_id) REFERENCES vendors(ven_id) ON DELETE RESTRICT ON UPDATE RESTRICT, FOREIGN KEY (sup_id) REFERENCES supplies(sup_id) ON DELETE RESTRICT ON UPDATE RESTRICT);";
if ($conn->query($pur) === TRUE) {
  echo "MySQL: Created Table <b>Sales</b> Successfully.<br />";
} else {
  echo "Error: ".$pur."<br>".$conn->error;
}

$ern = "CREATE TABLE earnings (ern_id int NOT NULL AUTO_INCREMENT, cus_id int NOT NULL, sal_id int NOT NULL, money decimal(10,5) NOT NULL, datetime datetime NOT NULL DEFAULT CURRENT_TIMESTAMP, PRIMARY KEY (ern_id), FOREIGN KEY (cus_id) REFERENCES customers(cus_id) ON DELETE RESTRICT ON UPDATE RESTRICT, FOREIGN KEY (sal_id) REFERENCES sales(sal_id) ON DELETE RESTRICT ON UPDATE RESTRICT);";
if ($conn->query($ern) === TRUE) {
  echo "MySQL: Created Table <b>Earnings</b> Successfully.<br />";
} else {
  echo "Error: ".$ern."<br>".$conn->error;
}

$spn = "CREATE TABLE spendings (spn_id int NOT NULL AUTO_INCREMENT, ven_id int NOT NULL, pur_id int NOT NULL, money decimal(10,5) NOT NULL, datetime datetime NOT NULL DEFAULT CURRENT_TIMESTAMP, PRIMARY KEY (spn_id), FOREIGN KEY (ven_id) REFERENCES vendors(ven_id) ON DELETE RESTRICT ON UPDATE RESTRICT, FOREIGN KEY (pur_id) REFERENCES purchases(pur_id) ON DELETE RESTRICT ON UPDATE RESTRICT);";
if ($conn->query($spn) === TRUE) {
  echo "MySQL: Created Table <b>Spendings</b> Successfully.<br />";
} else {
  echo "Error: ".$spn."<br>".$conn->error;
}

$ernid = "ALTER TABLE sales ADD FOREIGN KEY (ern_id) REFERENCES earnings(ern_id);";
if ($conn->query($ernid) === TRUE) {
  echo "MySQL: Connected Table <b>Sales</b> And Table <b>Earnings</b> Successfully.<br />";
} else {
  echo "Error: ".$ernid."<br>".$conn->error;
}

$spnid = "ALTER TABLE purchases ADD FOREIGN KEY (spn_id) REFERENCES spendings(spn_id);";
if ($conn->query($spnid) === TRUE) {
  echo "MySQL: Connected Table <b>Purchases</b> And Table <b>Spendings</b> Successfully.<br />";
} else {
  echo "Error: ".$spnid."<br>".$conn->error;
}

$salcus = "CREATE TRIGGER `salcus` BEFORE INSERT ON `sales` FOR EACH ROW UPDATE customers SET balance = balance - NEW.money WHERE cus_id = NEW.cus_id;";
if ($conn->query($salcus) === TRUE) {
  echo "MySQL: Created Trigger SalCus Between Table <b>Sales</b> And Table <b>Customers</b> Successfully.<br />";
} else {
  echo "Error: ".$salcus."<br>".$conn->error;
}

$purven = "CREATE TRIGGER `purven` BEFORE INSERT ON `purchases` FOR EACH ROW UPDATE vendors SET balance = balance + NEW.money WHERE ven_id = NEW.ven_id;";
if ($conn->query($purven) === TRUE) {
  echo "MySQL: Created Trigger PurVen Between Table <b>Vendors</b> And Table <b>Purchases</b> Successfully.<br />";
} else {
  echo "Error: ".$purven."<br>".$conn->error;
}

$ernsal = "CREATE TRIGGER `ernsal` BEFORE INSERT ON `earnings` FOR EACH ROW UPDATE sales SET ern_id = NEW.ern_id WHERE sal_id = NEW.sal_id;";
if ($conn->query($ernsal) === TRUE) {
  echo "MySQL: Created Trigger ErnSal Between Table <b>Earnings</b> And Table <b>Sales</b> Successfully.<br />";
} else {
  echo "Error: ".$ernsal."<br>".$conn->error;
}

$spnpur = "CREATE TRIGGER `spnpur` BEFORE INSERT ON `spendings` FOR EACH ROW UPDATE purchases SET spn_id = NEW.spn_id WHERE pur_id = NEW.pur_id;";
if ($conn->query($spnpur) === TRUE) {
  echo "MySQL: Created Trigger SpnPur Between Table <b>Spendings</b> And Table <b>Purchases</b> Successfully.<br />";
} else {
  echo "Error: ".$spnpur."<br>".$conn->error;
}

$conn->close();
?>
