<?php
$conn = new mysqli('localhost', 'usertest', 'passtest', 'test1');
if ($conn->connect_error) {
  die("MySQL Connection failed: " . $conn->connect_error);
} else {
  echo "MySQL Connected Successfully.";
}

$sql = "DROP TABLE IF EXISTS supplies, earnings, spendings, customers, vendors, sales, purchases, products, ratelists, accounts;";
if ($conn->query($sql) === TRUE) {
  echo "MySQL: Dropped <b>All</b> Tables Successfully.<br />";
} else {
  echo "Error: ".$sql."<br>".$conn->error;
}

$conn->close();
?>
