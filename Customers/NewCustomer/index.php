<!DOCTYPE html>
<html>
<head>
  <title>New Customers - MilkMaid</title>
  <link rel="icon" type="image/x-icon" href="../../milkmaid-logo.png">
  <link rel="stylesheet" href="../../CSS/common.css">
</head>
<body>
<div class="inputbox">
<button class="column backred" onclick="location.href = '../';">←</button>
<form class="column inputform" action="inject.php" method="POST">
    <label for="name">Name:</label><br>
    <input type="text" name="name" required><br>

    <label for="address">Address:</label><br>
    <input type="text" name="address" required><br>

    <label for="ratefor">Rate Formula:</label><br>
    <select id="ratefor" name="ratefor" required>
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
    </select><br>

    <label for="email">Enter your email:</label><br>
    <input type="email" id="email" name="email"><br>

    <label for="phone">Enter your phone number:</label><br>
    <input type="tel" id="phone" name="phone" pattern="[0-9]{10}"><br>

    <input type="submit">
</form>
</div>
</body>
</html>
